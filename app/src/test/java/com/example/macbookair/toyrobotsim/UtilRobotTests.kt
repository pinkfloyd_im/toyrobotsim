package com.example.macbookair.toyrobotsim

import com.example.macbookair.toyrobotsim.backend.CommandParser
import com.example.macbookair.toyrobotsim.robot.SimRobot
import com.example.macbookair.toyrobotsim.world.*
import org.junit.Test

import org.junit.Assert.*

/**
 * Testing for sending commands to the robot.
 *
 * So we can ensure the commands functions as expected.
 * Before we integrate the robot into User Interface.
 */
class UtilRobotTests {
    /*
        Used to setup the Simulator world
     */
    private fun setup(): SimRobot {
        val grid = SimGrid(5, 5)
        val world = SimWorld(grid)

        return  SimRobot(world)
    }

    @Test
    /*
    Check commands are ignored if ROBOT is not on grid.
     */
    fun utilCommandIgnored() {
        //Place the Robot
        val robot = setup()
        assertEquals(SimCommand.move(robot), "ROBOT not on grid")
    }

    @Test
    /*
    Check parser validates
     */
    fun utilParserValid() {
        assertEquals(CommandParser.validCommand("PLACE 0,0,N"), true)
    }

    @Test
    /*
    Check parser fails
     */
    fun utilParserInvalid() {
        assertEquals(CommandParser.validCommand("PLACE 5,6"), false)
    }

    @Test
    /*
    Check parser doesnt crash
     */
    fun utilParserNoCrash() {
        assertEquals(CommandParser.validCommand("PLACE _0,S,0"), false)
    }
}
