package com.example.macbookair.toyrobotsim.listing

class ConsoleData(var cmd: String, var code: String) {
    var command: String
    var response: String

    init {
        command = cmd
        response = code
    }
}