package com.example.macbookair.toyrobotsim

import com.example.macbookair.toyrobotsim.robot.SimRobot
import com.example.macbookair.toyrobotsim.world.*
import org.junit.Assert
import org.junit.Test

class PlaceRobotTests {
    /*
        Used to setup the Simulator world
     */
    private fun setup(): SimRobot {
        val grid = SimGrid(5, 5)
        val world = SimWorld(grid)

        return  SimRobot(world)
    }

    @Test
    /*
        Check that our placement code returns what we expect
     */
    fun placeCommandValid() {
        //Place the Robot
        val robot = setup()
        val pos = SimLocation(0, 0)
        val dir = SimDirection.NORTH
        SimCommand.place(pos, dir, robot)
        Assert.assertEquals(SimCommand.report(robot), "0,0,NORTH")
    }

    @Test
    /*
    Test if we load the robot outside of the grid,
    that it will tell us that it is invalid.
     */
    fun placeCommandInvalid() {
        //Place the Robot
        val robot = setup()
        val pos = SimLocation(-1, 0)
        val dir = SimDirection.NORTH
        Assert.assertEquals(SimCommand.place(pos, dir, robot), "invalid ROBOT placement")
    }

    @Test
    /*
    Make sure that the robot location is not invalid,
    after a second PLACE command with invalid data is executed.
     */
    fun placeCommandSequence() {
        val robot = setup()
        SimCommand.place(SimLocation(0, 0), SimDirection.NORTH, robot)
        SimCommand.place(SimLocation(-1, 0), SimDirection.WEST, robot)
        Assert.assertEquals(SimCommand.report(robot), "0,0,NORTH")
    }
}