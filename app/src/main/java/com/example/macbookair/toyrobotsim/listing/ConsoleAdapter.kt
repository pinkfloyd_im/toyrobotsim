package com.example.macbookair.toyrobotsim.listing

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.macbookair.toyrobotsim.R
import kotlinx.android.synthetic.main.console_item.view.*

class ConsoleAdapter(val items : ArrayList<ConsoleData>, val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.console_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        holder?.consoleResponse?.text = items.get(position).response
    }
}

class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val consoleResponse = view.consoleTitle
}
