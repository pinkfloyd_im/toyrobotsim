package com.example.macbookair.toyrobotsim

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.macbookair.toyrobotsim.backend.CommandParser
import com.example.macbookair.toyrobotsim.backend.DataManager
import com.example.macbookair.toyrobotsim.listing.ConsoleAdapter
import com.example.macbookair.toyrobotsim.robot.SimRobot
import com.example.macbookair.toyrobotsim.world.*

/*
This is were the application loads.

The User Interface:

We are using a search view to send commands to the robot.
Each command gives feedback to the user

Future Work:

Could add a graphical interface to see the robot move
Could be represented by a DxD grid, and a animated robot icon

 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val ctx: Context = this

        //Make Data Store
        var dataStore = DataManager()

        //Setup World
        val grid = SimGrid(5, 5)
        val world = SimWorld(grid)

        //Add the Robot
        val robot = SimRobot(world)

        //Setup List
        val searchListing = findViewById<RecyclerView>(R.id.searchListing)
        searchListing.layoutManager = LinearLayoutManager(this)
        searchListing.layoutManager = GridLayoutManager(this, 1) as RecyclerView.LayoutManager?

        //User Search
        val searchRequest = findViewById<SearchView>(R.id.searchRequest)
        searchRequest.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                //Parse Command
                if (query == "?") { //HELP
                    dataStore.doHelp {
                        this@MainActivity.runOnUiThread(java.lang.Runnable {
                            searchListing.adapter = it?.let { it1 -> ConsoleAdapter(it1, ctx) }
                        })
                    }
                }
                else { //Robot Command
                    CommandParser.parseCommand(query, robot) {
                        this@MainActivity.runOnUiThread(java.lang.Runnable {
                            searchListing.adapter = it?.let { it1 -> ConsoleAdapter(it1, ctx) }
                        })
                    }
                }

                return false
            }
        })
    }
}
