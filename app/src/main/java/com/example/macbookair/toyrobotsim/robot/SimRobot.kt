package com.example.macbookair.toyrobotsim.robot

import android.util.Log
import com.example.macbookair.toyrobotsim.world.SimDirection
import com.example.macbookair.toyrobotsim.world.SimLocation
import com.example.macbookair.toyrobotsim.world.SimWorld

class SimRobot(var world: SimWorld) {
    private var grid: SimWorld
    private var position: SimLocation? = null
    private var direction: SimDirection? = null

    init {
        grid = world
    }

    fun hasPosition(): Boolean {
        position?.let {
            return true
        }

        //default
        return false
    }

    fun robotPosition(): SimLocation? {
        return position
    }

    fun place(moveTo: SimLocation, inDirection: SimDirection): Boolean {
        //If placement succeeds
        if (grid.place(moveTo, inDirection)) {
            position = moveTo
            direction = inDirection

            return true
        }

        //default
        return false
    }

    fun report(): String {
        var pos = "unknown"
        var dir = "unknown"

        //Unwrap optionals
        position?.let {
            pos = it.display() as String
        }
        direction?.let {
            dir = it.name as String
        }

        return "$pos,$dir"
    }

    fun move() {
        position = position?.let { direction?.let { it1 -> grid.move(it, it1) } }
    }

    fun left() {
        when (direction) {
            SimDirection.NORTH -> {
                direction = SimDirection.WEST
            }
            SimDirection.SOUTH -> {
                direction = SimDirection.EAST
            }
            SimDirection.EAST -> {
                direction = SimDirection.NORTH
            }
            SimDirection.WEST -> {
                direction = SimDirection.SOUTH
            }
        }
    }

    fun right() {
        when (direction) {
            SimDirection.NORTH -> {
                direction = SimDirection.EAST
            }
            SimDirection.SOUTH -> {
                direction = SimDirection.WEST
            }
            SimDirection.EAST -> {
                direction = SimDirection.SOUTH
            }
            SimDirection.WEST -> {
                direction = SimDirection.NORTH
            }
        }
    }
}