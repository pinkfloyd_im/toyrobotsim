package com.example.macbookair.toyrobotsim.backend

import com.example.macbookair.toyrobotsim.listing.ConsoleData

/*
The purpose of this class is to maintain the help system.
 */
class DataManager {
    private var data = ArrayList<ConsoleData>()

    fun doHelp(callback: (result: ArrayList<ConsoleData>?) -> Unit) {
        data.clear()
        data.add(ConsoleData("", "PLACE X,Y,F (e.g PLACE 0,0,N)"))
        data.add(ConsoleData("", "MOVE"))
        data.add(ConsoleData("", "LEFT"))
        data.add(ConsoleData("", "RIGHT"))
        data.add(ConsoleData("", "REPORT"))
        callback.invoke(data)
    }
}