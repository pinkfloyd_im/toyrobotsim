package com.example.macbookair.toyrobotsim.world

/*
This class maintains the world grid.
Has the ability to be extended to be a 3d world if required.
Currently the height = 0, so the world is 2d.
As the grid knows its bounds can control edge detection.
 */
class SimGrid(var grid_width: Int, var grid_depth: Int, var grid_height: Int = 0) {
    private var width: Int
    private var depth: Int
    private var height: Int

    init {
        width = grid_width
        depth = grid_depth
        height = grid_height
    }

    fun place(moveTo: SimLocation, inDirection: SimDirection): Boolean {
        //Test down
        if (moveTo.location("x") < 0 || moveTo.location("x") > maxDim("width")) {
            return false
        }

        //Test across
        if (moveTo.location("y") < 0 || moveTo.location("y") > maxDim("depth")) {
            return false
        }

        //Test up
        if (moveTo.location("z") < 0 || moveTo.location("z") > maxDim("height")) {
            return false
        }

        //default
        return true
    }

    fun move(currentPos: SimLocation, currentDirection: SimDirection): SimLocation {
        var newLocation = SimLocation(currentPos.location("x"), currentPos.location("y"))
        when (currentDirection) {
            SimDirection.NORTH -> {
                newLocation.updateLocation(newLocation.location("x"), newLocation.location("y")+1)
            }
            SimDirection.SOUTH -> {
                newLocation.updateLocation(newLocation.location("x"), newLocation.location("y")-1)
            }
            SimDirection.EAST -> {
                newLocation.updateLocation(newLocation.location("x")+1, newLocation.location("y"))
            }
            SimDirection.WEST -> {
                newLocation.updateLocation(newLocation.location("x")-1, newLocation.location("y"))
            }
        }

        //Test the new location end points
        if (place(newLocation, currentDirection)) {
            return newLocation
        }

        //default
        return currentPos
    }

    private fun maxDim(type: String): Int {
        when (type) {
            "width" -> {
                return width-1
            }
            "depth" -> {
                return depth-1
            }
        }

        //default
        return height
    }
}