package com.example.macbookair.toyrobotsim.world

class SimLocation(var loc_x: Int, var loc_y: Int, var loc_z: Int = 0) {
    private var x: Int
    private var y: Int
    private var z: Int

    init {
        x = loc_x
        y = loc_y
        z = loc_z
    }

    fun display(): String {
        return "$x,$y"
    }

    fun updateLocation(location_x: Int, location_y: Int) {
        x = location_x
        y = location_y
    }

    /*
    Allow access to the private data.
     */
    fun location(type: String): Int {
        when (type) {
            "x" -> {
                return x
            }
            "y" -> {
                return y
            }
        }

        //default
        return z
    }
}