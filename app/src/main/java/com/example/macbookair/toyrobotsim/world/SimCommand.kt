package com.example.macbookair.toyrobotsim.world

import com.example.macbookair.toyrobotsim.robot.SimRobot

/*
This class is used to send commands to a robot.
The system can actually have more than 1 robot loaded onto the world.
 */
class SimCommand {
    companion object Factory {
        fun place(position: SimLocation, direction: SimDirection, robot: SimRobot): String {
            if (robot.place(position, direction)) {
                return "ROBOT placed"
            }

            //default
            return "invalid ROBOT placement"
        }

        /*
        MOVE: Will move the toy robot one unit forward in the direction it is currently facing.
         */
        fun move(robot: SimRobot): String {
            if (robot.hasPosition()) {
                val lastLocation = robot.robotPosition()
                robot.move()
                val newLocation = robot.robotPosition()

                //check if we moved
                if (lastLocation == newLocation) {
                    return "ROBOT hit a edge"
                }

                return "ROBOT moved 1 unit"
            }

            //default
            return "ROBOT not on grid"
        }

        /*
        LEFT: Will rotate the robot 90 degrees in the specified direction
        without changing the position of the robot.
         */
        fun left(robot: SimRobot): String {
            if (robot.hasPosition()) {
                robot.left()

                return "ROBOT turned left"
            }

            //default
            return "ROBOT not on grid"
        }

        /*
        RIGHT: Will rotate the robot 90 degrees in the specified direction without
        changing the position of the robot.
         */
        fun right(robot: SimRobot): String {
            if (robot.hasPosition()) {
                robot.right()

                return "ROBOT turned right"
            }

            //default
            return "ROBOT not on grid"
        }

        /*
        REPORT: Will announce the X,Y and F of the robot. This can be in any form,
        but standard output is sufficient.
         */
        fun report(robot: SimRobot): String {
            if (robot.hasPosition()) {
                return robot.report()
            }

            //default
            return "ROBOT not on grid"
        }
    }
}