package com.example.macbookair.toyrobotsim.world

enum class SimDirection {
    NORTH, SOUTH, WEST, EAST
}