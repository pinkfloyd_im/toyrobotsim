package com.example.macbookair.toyrobotsim

import com.example.macbookair.toyrobotsim.robot.SimRobot
import com.example.macbookair.toyrobotsim.world.*
import org.junit.Assert
import org.junit.Test

class PathRobotTests {
    /*
        Used to setup the Simulator world
     */
    private fun setup(): SimRobot {
        val grid = SimGrid(5, 5)
        val world = SimWorld(grid)

        return  SimRobot(world)
    }

    /*
        Setup a default start location (bottom left)
     */
    private fun setStart(direction: SimDirection): SimRobot {
        val robot = setup()
        val pos = SimLocation(0, 0)
        val dir = direction
        SimCommand.place(pos, dir, robot)

        return  robot
    }

    @Test
    /*
        Move to the top right corner
        Robot starts at: 0,0 (bottom left)
     */
    fun pathTopRight() {
        val robot = setStart(SimDirection.NORTH)
        //Move 4 up
        for (x in 0..3) {
            SimCommand.move(robot)
        }
        SimCommand.right(robot)
        //Move 4 across
        for (x in 0..3) {
            SimCommand.move(robot)
        }
        Assert.assertEquals(SimCommand.report(robot), "4,4,EAST")
    }

    @Test
    /*
        Move to the middle of the grid
     */
    fun pathMiddle() {
        val robot = setStart(SimDirection.NORTH)
        //Move 2 up
        for (x in 0..1) {
            SimCommand.move(robot)
        }
        SimCommand.right(robot)
        //Move 2 across
        for (x in 0..1) {
            SimCommand.move(robot)
        }
        SimCommand.left(robot)
        Assert.assertEquals(SimCommand.report(robot), "2,2,NORTH")
    }

    @Test
    /*
        Move to top left, using snake pattern
        Robot starts at: 0,0 (bottom left)
     */
    fun pathTopLeft() {
        val robot = setStart(SimDirection.NORTH)
        SimCommand.move(robot)
        SimCommand.right(robot)
        SimCommand.move(robot)
        SimCommand.left(robot)
        SimCommand.move(robot)
        SimCommand.right(robot)
        SimCommand.move(robot)
        SimCommand.left(robot)
        SimCommand.move(robot)
        SimCommand.right(robot)
        SimCommand.move(robot)
        SimCommand.left(robot)
        SimCommand.move(robot)
        SimCommand.left(robot)
        //Move 2 back
        for (x in 0..3) {
            SimCommand.move(robot)
        }
        Assert.assertEquals(SimCommand.report(robot), "0,4,WEST")
    }

    @Test
    /*
        Fly off edge, random test
     */
    fun pathFlyOffEdge() {
        val robot = setStart(SimDirection.NORTH)
        SimCommand.move(robot)
        SimCommand.right(robot)
        //Move 30 across
        for (x in 0..30) {
            SimCommand.move(robot)
        }
        Assert.assertEquals(SimCommand.report(robot), "4,1,EAST")
    }

    @Test
    /*
        Dual test
        Place two robots on the world,
        and place move them to face each other.
     */
    fun pathDual() {
        //Place Robot 1
        val robot1 = setStart(SimDirection.NORTH)
        SimCommand.move(robot1)
        SimCommand.move(robot1)
        SimCommand.right(robot1)
        SimCommand.move(robot1)
        //Place Robot 2
        val robot2 = setStart(SimDirection.EAST)
        SimCommand.move(robot2)
        SimCommand.move(robot2)
        SimCommand.move(robot2)
        SimCommand.left(robot2)
        SimCommand.move(robot2)
        SimCommand.move(robot2)
        SimCommand.left(robot2)
        //Dual (They are facing each other)
        Assert.assertEquals(SimCommand.report(robot1), "1,2,EAST")
        Assert.assertEquals(SimCommand.report(robot2), "3,2,WEST")
    }
}