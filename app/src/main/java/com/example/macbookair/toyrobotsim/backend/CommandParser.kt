package com.example.macbookair.toyrobotsim.backend

import com.example.macbookair.toyrobotsim.listing.ConsoleData
import com.example.macbookair.toyrobotsim.robot.SimRobot
import com.example.macbookair.toyrobotsim.world.SimCommand
import com.example.macbookair.toyrobotsim.world.SimDirection
import com.example.macbookair.toyrobotsim.world.SimLocation

class CommandParser {
    companion object Factory {
        /*
        Make sure we have:
        PLACE X,Y,F
        LEFT
        RIGHT
        REPORT
         */
        fun validCommand(cmd: String): Boolean {
            when (cmd.toUpperCase()) {
                "LEFT" -> {
                    return true
                }
                "RIGHT" -> {
                    return true
                }
                "MOVE" -> {
                    return true
                }
                "REPORT" -> {
                    return true
                }
            }

            //Place Validator
            var placeCmd = cmd.toUpperCase()
            if (placeCmd.contains("PLACE")) {
                placeCmd = placeCmd.replace("PLACE", "")
                placeCmd = placeCmd.trim()
                val parts = placeCmd.split(",")
                if (parts.size == 3) {
                    if (parts[0].toIntOrNull() == null) {
                        return false
                    }
                    if (parts[1].toIntOrNull() == null) {
                        return false
                    }
                    if (parts[2].toUpperCase() != "N" &&
                        parts[2].toUpperCase() != "S" &&
                        parts[2].toUpperCase() != "E" &&
                        parts[2].toUpperCase() != "W") {
                        return false
                    }

                    //default
                    return true
                }
            }

            //default
            return false
        }

        /*
        Execute the command, and get the feedback.
        Add the feedback to the callback.
        Need to use 'return' to prevent unwanted code execution.
        The default is to give: invalid command
         */
        fun parseCommand(cmd: String, robot: SimRobot, callback: (result: ArrayList<ConsoleData>?) -> Unit) {
            var results = ArrayList<ConsoleData>()
            if (validCommand(cmd)) {
                when (cmd.toUpperCase()) {
                    "LEFT" -> {
                        val console = SimCommand.left(robot)
                        results.add(ConsoleData(cmd, console))
                        callback.invoke(results)
                        return
                    }
                    "RIGHT" -> {
                        val console = SimCommand.right(robot)
                        results.add(ConsoleData(cmd, console))
                        callback.invoke(results)
                        return
                    }
                    "REPORT" -> {
                        val console = SimCommand.report(robot)
                        results.add(ConsoleData(cmd, console))
                        callback.invoke(results)
                        return
                    }
                    "MOVE" -> {
                        val console = SimCommand.move(robot)
                        results.add(ConsoleData(cmd, console))
                        callback.invoke(results)
                        return
                    }
                }

                //Place
                var placeCmd = cmd.toUpperCase()
                if (placeCmd.contains("PLACE")) {
                    placeCmd = placeCmd.replace("PLACE", "")
                    placeCmd = placeCmd.trim()
                    val parts = placeCmd.split(",")
                    when (parts[2] as String) {
                        "N" -> {
                            val console = SimCommand.place(SimLocation(parts[0].toInt(), parts[1].toInt()), SimDirection.NORTH, robot)
                            results.add(ConsoleData(cmd, console))
                            callback.invoke(results)
                            return
                        }
                        "S" -> {
                            val console = SimCommand.place(SimLocation(parts[0].toInt(), parts[1].toInt()), SimDirection.SOUTH, robot)
                            results.add(ConsoleData(cmd, console))
                            callback.invoke(results)
                            return
                        }
                        "E" -> {
                            val console = SimCommand.place(SimLocation(parts[0].toInt(), parts[1].toInt()), SimDirection.EAST, robot)
                            results.add(ConsoleData(cmd, console))
                            callback.invoke(results)
                            return
                        }
                        "W" -> {
                            val console = SimCommand.place(SimLocation(parts[0].toInt(), parts[1].toInt()), SimDirection.WEST, robot)
                            results.add(ConsoleData(cmd, console))
                            callback.invoke(results)
                            return
                        }
                    }
                }

                //default
                results.add(ConsoleData(cmd, "invalid command: check syntax"))
                callback.invoke(results)
            }
            else {
                //default
                results.add(ConsoleData(cmd, "invalid command: check syntax"))
                callback.invoke(results)
            }
        }
    }
}