TOOLS:

Android Studio 3.6
Kotlin

HOW TO BUILD:

Use the above tool, and open the andorid project.
Press `Play` button in toolbar to build.
Assues you have selected a device or virtual machine.

USAGE:

Press the `search` icon, and enter a command:

? - List of commands is displayed
PLACE X,Y,F - e.g PLACE 0,0,N
LEFT - Robot turns left
RIGHT - Robot turns right
MOVE - Robot moves 1 unit in current direction
REPORT - Display Robot postion, and direction

Assumes you press `search` icon on keyboard to run the command.

TEST:

Move Tests - Checks if the robot moves correctly within the grid.
Path Tests - Create various paths and confirm expected results.
Place Tests - Check the initial placement on grid functions.
Util Tests - Checks various utilities used by the application.



