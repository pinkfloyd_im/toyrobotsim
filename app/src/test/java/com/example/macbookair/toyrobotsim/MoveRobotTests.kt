package com.example.macbookair.toyrobotsim

import com.example.macbookair.toyrobotsim.robot.SimRobot
import com.example.macbookair.toyrobotsim.world.*
import org.junit.Assert
import org.junit.Test

class MoveRobotTests {
    /*
        Used to setup the Simulator world
     */
    private fun setup(): SimRobot {
        val grid = SimGrid(5, 5)
        val world = SimWorld(grid)

        return  SimRobot(world)
    }

    /*
        Setup a default start location (bottom left)
     */
    private fun setStart(direction: SimDirection): SimRobot {
        val robot = setup()
        val pos = SimLocation(0, 0)
        val dir = direction
        SimCommand.place(pos, dir, robot)

        return  robot
    }

    @Test
    /*
        Test if we can move north correctly
     */
    fun moveNorth() {
        val robot = setStart(SimDirection.NORTH)
        SimCommand.move(robot)
        SimCommand.move(robot)
        Assert.assertEquals(SimCommand.report(robot), "0,2,NORTH")
    }

    @Test
    /*
        Test north edge case
     */
    fun moveNorthEdge() {
        val robot = setStart(SimDirection.NORTH)
        //Loop Over data
        for (x in 0..10) {
            SimCommand.move(robot)
        }
        Assert.assertEquals(SimCommand.report(robot), "0,4,NORTH")
    }

    @Test
    /*
        Test West edge case
     */
    fun moveWestEdge() {
        val robot = setStart(SimDirection.WEST)
        SimCommand.move(robot)
        Assert.assertEquals(SimCommand.report(robot), "0,0,WEST")
    }

    @Test
    /*
        Test South edge case
     */
    fun moveSouthEdge() {
        val robot = setStart(SimDirection.SOUTH)
        SimCommand.move(robot)
        Assert.assertEquals(SimCommand.report(robot), "0,0,SOUTH")
    }

    @Test
    /*
        Test East move case
     */
    fun moveEast() {
        val robot = setStart(SimDirection.EAST)
        SimCommand.move(robot)
        Assert.assertEquals(SimCommand.report(robot), "1,0,EAST")
    }

    @Test
    /*
        Test east edge case
     */
    fun moveEastEdge() {
        val robot = setStart(SimDirection.EAST)
        //Loop Over data
        for (x in 0..10) {
            SimCommand.move(robot)
        }
        Assert.assertEquals(SimCommand.report(robot), "4,0,EAST")
    }

    @Test
    /*
    Sample 1: PLACE 0,0,NORTH MOVE REPORT Output: 0,1,NORTH
     */
    fun moveSample1() {
        val robot = setup()
        SimCommand.place(SimLocation(0,0), SimDirection.NORTH, robot)
        SimCommand.move(robot)
        Assert.assertEquals(SimCommand.report(robot), "0,1,NORTH")
    }

    @Test
    /*
    Sample 2: PLACE 0,0,NORTH LEFT REPORT Output: 0,0,WEST
     */
    fun moveSample2() {
        val robot = setup()
        SimCommand.place(SimLocation(0,0), SimDirection.NORTH, robot)
        SimCommand.left(robot)
        Assert.assertEquals(SimCommand.report(robot), "0,0,WEST")
    }

    @Test
    /*
    Sample 3: PLACE 1,2,EAST MOVE MOVE LEFT MOVE REPORT Output: 3,3,NORTH
     */
    fun moveSample3() {
        val robot = setup()
        SimCommand.place(SimLocation(1,2), SimDirection.EAST, robot)
        SimCommand.move(robot)
        SimCommand.move(robot)
        SimCommand.left(robot)
        SimCommand.move(robot)
        Assert.assertEquals(SimCommand.report(robot), "3,3,NORTH")
    }
}