package com.example.macbookair.toyrobotsim.world

/*
This class represents the world, which holds a grid size.
Can be extended to have more features.
Examples: More than 1 grid
 */
class SimWorld(val worldSize: SimGrid) {
    private var dims: SimGrid = worldSize

    fun place(moveTo: SimLocation, inDirection: SimDirection): Boolean {
        return dims.place(moveTo, inDirection)
    }

    fun move(currentPos: SimLocation, currentDirection: SimDirection): SimLocation {
        return dims.move(currentPos, currentDirection)
    }
}